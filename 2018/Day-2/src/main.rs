use std::fs::File;
use std::io::{BufRead, BufReader, Result};

fn main() -> Result<()> {
    let file = File::open("input.txt")?;
    let chars = "abcdefghjiklmnopqrstuwvxyz";

    let mut two_chr = 0;
    let mut three_chr = 0;
    for line in BufReader::new(file).lines() {
        // println!("{}", line?);
        let cur_line = line?;
        println!("{}", cur_line);
        let mut found_two = false;
        let mut found_three = false;
        let mut it = chars.chars().peekable();

        loop {
            match it.peek() {
                Some(&ch) => {
                    let chunks: Vec<_> = cur_line.split(ch).collect();

                    let len = chunks.len() - 1;

                    if found_three && found_two {
                        break;
                    }

                    if len == 3 && !found_three {
                        three_chr += 1;
                        found_three = true;

                        println!("{} {}", len, ch);
                    } else if len == 2 && !found_two {
                        found_two = true;
                        two_chr += 1;

                        println!("{} {}", len, ch);
                    }

                    it.next().unwrap();
                }
                None => break,
            }
        }
    }

    println!("{} * {} = {}", two_chr, three_chr, (two_chr * three_chr));
    Ok(())
}

