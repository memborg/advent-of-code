use std::fs::File;
use std::io::{BufRead, BufReader, Result};

fn main() -> Result<()> {
    let file = File::open("input.txt")?;
    let mut cur_frq = 0;
    for line in BufReader::new(file).lines() {
        // println!("{}", line?);
        let change = line?.clone().parse::<i32>().unwrap();
        // println!("{} += {}", cur_frq, change);
        cur_frq += change;
    }

    println!("{}", cur_frq);
    Ok(())
}

